import java.util.*;


class RouteComparator implements Comparator<Route> {
    @Override
    public int compare(Route a, Route b) {
        if (a.getNetwork().getMaskLength() == b.getNetwork().getMaskLength())
            return a.getMetric() - b.getMetric();
        else
            return b.getNetwork().getMaskLength() - a.getNetwork().getMaskLength();
    }
}

public class Router {
    private TreeSet<Route> routes;
    private List<Route> sortedRoutes;

    public Router(Iterable<Route> routes) {
        this.routes = new TreeSet<>(new RouteComparator());

        for (Route route : routes) {
            this.routes.add(route);
        }
    }

    public void addRoute(Route route) {
        this.routes.add(route);
    }

    public Route getRouteForAddress(IPv4Address address) {
        for (Route route : this.routes)
            if (route.getNetwork().contains(address))
                return route;
        return null;
    }

    public Iterable<Route> getRoutes() {
        return this.routes;
    }

    public void removeRoute(Route route) {
        this.routes.remove(route);
    }

    public static void main(String[] args) {
        @SuppressWarnings("serial")
        List<Route> routes = new ArrayList<Route>() {{
            add(new Route(new Network(new IPv4Address("0.0.0.0"), 0), new IPv4Address("192.168.0.1"), "en0", 1));
            add(new Route(new Network(new IPv4Address("192.168.0.0"), 24), null, "en0", 2));
            add(new Route(new Network(new IPv4Address("192.168.0.0"), 25), null, "en1", 5));
            add(new Route(new Network(new IPv4Address("192.168.0.0"), 25), null, "en1", 7));
            add(new Route(new Network(new IPv4Address("10.0.0.0"), 8), new IPv4Address("10.123.0.1"), "en1", 11));
            add(new Route(new Network(new IPv4Address("10.0.0.0"), 8), new IPv4Address("10.123.0.1"), "en1", 10));
            add(new Route(new Network(new IPv4Address("10.123.0.0"), 20), null, "en1", 4));
        }};

        Router router = new Router(routes);

        Route a = new Route(new Network(new IPv4Address("10.111.0.5"), 20), null, "en1", 9);
        router.addRoute(a);
        router.addRoute(new Route(new Network(new IPv4Address("10.0.0.0"), 8), new IPv4Address("10.123.0.1"), "en1", 9));
        router.removeRoute(a);

        Route route = router.getRouteForAddress(new IPv4Address("192.168.0.076"));
        System.out.println(route);
        System.out.println(route.getMetric());                  // 5
        System.out.println(route.getInterfaceName());           // en1
        Network net = route.getNetwork();
        System.out.println(net.toString());                     // 192.168.0.0/25
        System.out.println(net.getAddress().toString());        // 192.168.0.0

        System.out.println("---------------------------------");
        route = router.getRouteForAddress(new IPv4Address("10.0.0.0"));
        System.out.println(route);
        System.out.println(route.getMetric());                  // 9
        System.out.println(route.getInterfaceName());           // en1
        net = route.getNetwork();
        System.out.println(net.toString());                     // 10.0.0.0/8

        System.out.println(router.getRoutes());


        // // for ( Route r : router.getRoutes() ) {
        // //   	Network n = r.getNetwork();
        // // 	System.out.println(n);
        //    // other important operations
        // // }
    }
}
