import java.lang.*;

public class IPv4Address {
    private Long address_long;
    private String address_str;

    public IPv4Address(String address) {
        this.address_long = str2long(address);
        this.address_str = address;
    }

    public IPv4Address(long address) {
        this.address_long = address;
        this.address_str = long2str(address);
    }

    public Long str2long(String address) {
        String[] parts = address.split("\\.");
        long result = 0;
        long multiple = (long)Math.pow(256, 3);

        for( String part : parts ) {
            int intPart = Integer.parseInt(part);

            if ( !(intPart >= 0 && intPart <= 0xFF) ) {
                throw new IllegalArgumentException();
            }

            result += intPart * multiple;
            multiple /= 256;
        }

        return result;
    }

    public String long2str(long address) {
        if ( !(address >= 0 && address <= 0xFFFFFFFFl) ) {
            throw new IllegalArgumentException();
        }

        long[] bytes = new long[4];
        bytes[0] = address & 0xFFl;
        bytes[1] = (address >> 8) & 0xFFl;
        bytes[2] = (address >> 16) & 0xFFl;
        bytes[3] = (address >> 24) & 0xFFl;

        return String.format("%d.%d.%d.%d", bytes[3], bytes[2], bytes[1], bytes[0]);

    }

    public String getAddress() {
        return toString();
    }
    
    public boolean lessThan(IPv4Address address) {
        return this.address_long < address.toLong();
    }

    public boolean greaterThan(IPv4Address address) {
        return this.address_long > address.toLong();
    }

    public boolean equals(IPv4Address address) {
        return this.address_long == address.toLong();
    }

    public String toString() {
        return this.address_str;
    }

    public long toLong() {
        return this.address_long;
    }

    public static void main(String[] args) throws IllegalArgumentException {
        IPv4Address ip = new IPv4Address("127.12.45.22");
        System.out.println(ip.toString());  // 127.12.45.22
        System.out.println(ip.toLong());    // 2131504406

        IPv4Address ip2 = new IPv4Address(2131504406l);
        System.out.println(ip2.toString());  // 127.12.45.22
        System.out.println(ip2.toLong());    // 2131504406

        System.out.println(ip2.equals(new IPv4Address("127.12.45.22")));        // true
        System.out.println(ip2.equals(new IPv4Address(2131504406L)));           // true
        System.out.println(ip2.equals(new IPv4Address(0xF834AD02L)));           // false
        System.out.println(ip2.equals(new IPv4Address("189.11.23.211")));       // false
        System.out.println(ip2.greaterThan(new IPv4Address("131.16.34.66")));   // false
        System.out.println(ip2.lessThan(new IPv4Address("131.16.34.66")));      // true
    }
}
