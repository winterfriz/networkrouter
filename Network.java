class Network {
    Network[] private_nets_ranges = null;

    private IPv4Address address;
    private int maskLength;
    private IPv4Address mask;
    private IPv4Address broadcast;

    public Network(IPv4Address address, int maskLength) {
        this.address = new IPv4Address(address.toLong() & ((2l<<31) - (2l<<(31-maskLength))));
        this.maskLength = maskLength;
        this.mask = new IPv4Address(((2l << 31)-1) - ((2l << (31-maskLength))-1));
        this.broadcast = new IPv4Address(((2l << (31-maskLength))-1) + this.address.toLong());
    }

    public boolean contains(IPv4Address address) {
        return this.broadcast.toLong() >= address.toLong()
                && address.toLong() >= this.address.toLong();
    }

    public IPv4Address getAddress() {
        return this.address;
    }

    public IPv4Address getBroadcastAddress() {
        return this.broadcast;
    }

    public IPv4Address getFirstUsableAddress() {
        if ( this.maskLength == 31)
            return new IPv4Address(this.address.toLong());
        if ( this.maskLength == 32)
            return null;
        return new IPv4Address(this.address.toLong()+1);
    }

    public IPv4Address getLastUsableAddress() {
        if ( this.maskLength == 31)
            return new IPv4Address(this.broadcast.toLong());
        if ( this.maskLength == 32)
            return null;
        return new IPv4Address(this.broadcast.toLong()-1);
    }

    public long getMask() {
        return this.mask.toLong();
    }

    public String getMaskString() {
        return this.mask.toString();
    }

    public int getMaskLength() {
        return this.maskLength;
    }

    public Network[] getSubnets() {
        int newMask = this.maskLength + 1;

        if ( this.maskLength == 32) {
            return null;
        }

        Network[] nets = new Network[2];

        nets[0] = new Network(this.address, newMask);
        nets[1] = new Network(new IPv4Address(this.broadcast.toLong()+1), newMask);
        return nets;
    }

    public long getTotalHosts() {
        if ( this.maskLength == 31)
            return 2;
        if ( this.maskLength == 32)
            return 0;
        return (2l << (31-this.maskLength))-2;
    }

    public boolean isPublic() {
        if ( private_nets_ranges == null ) {
            fillPrivateNetsRanges();
        }
        for ( Network net : private_nets_ranges ) {
            if ( net.getAddress().toLong() <= this.address.toLong()
                    && net.getBroadcastAddress().toLong() >= this.address.toLong() )
                return false;
        }
        return true;
    }

    private void fillPrivateNetsRanges() {
        private_nets_ranges = new Network[] {
                new Network(new IPv4Address("10.0.0.0"), 8),
                new Network(new IPv4Address("172.16.0.0"), 12),
                new Network(new IPv4Address("192.168.0.0"), 16)
        };
    }

    public String toString() {
        StringBuilder sBuilder = new StringBuilder(this.address.toString());

        sBuilder.append("/");
        sBuilder.append(this.maskLength);

        return sBuilder.toString();
    }

    public static void main(String[] args) {
        IPv4Address address = new IPv4Address("192.168.0.0");
        Network net = new Network(address, 24);

        System.out.println(net.toString());                                        // 192.168.0.0/24
        System.out.println(net.getAddress().toString());                           // 192.168.0.0
        System.out.println(net.getFirstUsableAddress().toString());                // 192.168.0.1
        System.out.println(net.getLastUsableAddress().toString());                 // 192.168.0.254
        System.out.println(net.getMask());                                         // 4294967040
        System.out.println(net.getMaskString());                                   // 255.255.255.0
        System.out.println(net.getMaskLength());                                   // 24
        System.out.println(net.isPublic());                                        // false
        System.out.println(net.contains(new IPv4Address("10.0.23.4")));            // false
        System.out.println(net.contains(new IPv4Address("192.168.0.25")));         // true
        System.out.println(net.getBroadcastAddress().toString());                  // 192.168.0.255
        System.out.println(net.getTotalHosts());

        System.out.println("---------------------------");
        Network[] subnets = net.getSubnets();

        System.out.println(subnets[0].toString());                                 // 192.168.0.0/25
        System.out.println(subnets[0].getAddress().toString());                    // 192.168.0.0
        System.out.println(subnets[0].getFirstUsableAddress().toString());         // 192.168.0.1
        System.out.println(subnets[0].getLastUsableAddress().toString());          // 192.168.0.126
        System.out.println(subnets[0].getMaskLength());                            // 25
        System.out.println(subnets[0].getTotalHosts());
        
        System.out.println("---------------------------");

        System.out.println(subnets[1].toString());                                 // 192.168.0.0/25
        System.out.println(subnets[1].getAddress().toString());                    // 192.168.0.0
        System.out.println(subnets[1].getFirstUsableAddress().toString());         // 192.168.0.1
        System.out.println(subnets[1].getLastUsableAddress().toString());          // 192.168.0.126
        System.out.println(subnets[1].getMaskLength());                            // 25
        System.out.println(subnets[1].getTotalHosts());
        // IPv4Address address = new IPv4Address("192.168.0.0");
        // Network net2 = new Network(address, 31);

        // System.out.println(net2.getTotalHosts());
        // System.out.println(net2.toString());
        // System.out.println(net2.getBroadcastAddress().toString()); 
        // System.out.println(net2.getFirstUsableAddress().toString());
        // System.out.println(net2.getLastUsableAddress().toString());
        // System.out.println(net2.getAddress().toString());    
        // System.out.println(net2.getMaskString()); 
        // System.out.println(net2.getMaskLength());
        // System.out.println(net2.getBroadcastAddress().toString());

        // Network[] subnets = net2.getSubnets();
        // System.out.println("-----------------------------");
        // System.out.println(subnets[0].getTotalHosts());
        // System.out.println(subnets[0].toString());
        // System.out.println(subnets[0].getBroadcastAddress().toString());
        // System.out.println(subnets[0].getFirstUsableAddress().toString());
        // System.out.println(subnets[0].getLastUsableAddress().toString());
    //     System.out.println("-----------------------------");
    //     System.out.println(subnets[1].getTotalHosts());
    //     System.out.println(subnets[1].toString());
    //     System.out.println(subnets[1].getBroadcastAddress().toString());
    //     System.out.println(subnets[1].getFirstUsableAddress().toString());
    //     System.out.println(subnets[1].getLastUsableAddress().toString());
    }
}
